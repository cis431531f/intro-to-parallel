#include <iostream>
#include <vector>

#include <cstdio>
#include <cmath>

int N = 10000;

int main() {

  std::vector<double> A;
  std::vector<double> B;
  std::vector<double> C;

  A.resize(N); B.resize(N); C.resize(N);

  // initalize arrays
  for (int i = 0; i < N; ++i) {
    A[i] = i;
    B[i] = 2*i;
  }

  // apply trig functions 
  for (int i = 0; i < N; ++i) {
    C[i] = atan(A[i]) / cos(B[i]);
  }

  // verification
  for (int i = 0; i < N; ++i) {
    if (C[i] - (atan(i) / cos(2*i)) > 1e-8) {
      printf("Verfication Failed: C[%d] = %f\n", i, C[i]);
      return 0; 
    }
  }

  printf("Verification Successful!\n");
}
