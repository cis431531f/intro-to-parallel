#include <chrono>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>

typedef std::chrono::high_resolution_clock Clock;

#define NRA 512                 /* number of rows in matrix A */
#define NCA 512                /* number of columns in matrix A */
#define NCB 512                  /* number of columns in matrix B */

int main (int argc, char *argv[]) 
{
  int	i, j, k;
  double	a[NRA][NCA],           /* matrix A to be multiplied */
          b[NCA][NCB],           /* matrix B to be multiplied */
          c[NRA][NCB];           /* result matrix C */

  /*** Initialize matrices ***/
  for (i=0; i<NRA; i++)
    for (j=0; j<NCA; j++)
      a[i][j]= i+j;
  for (i=0; i<NCA; i++)
    for (j=0; j<NCB; j++)
      b[i][j]= i*j;
  for (i=0; i<NRA; i++)
    for (j=0; j<NCB; j++)
      c[i][j]= 0;

  auto t1 = Clock::now();

  for (i=0; i<NRA; i++)    
  {
    for(j=0; j<NCB; j++)       
      for (k=0; k<NCA; k++)
        c[i][j] += a[i][k] * b[k][j];
  }

  auto t2 = Clock::now();

  /*** Print results ***/
  printf("******************************************************\n");
  double sum = 0;
  for (i=0; i<NRA; i++)
  {
    for (j=0; j<NCB; j++) 
      sum += c[i][j];
  }
  printf("Result Matrix sum: %f\n", sum);

  printf("******************************************************\n");
  printf ("Done.\n");

  std::cout << "Runtime: " 
    << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count()
    << " nanoseconds" << std::endl;

}
